#!/bin/bash
#set -x 

#Instary bitbucket key to known hosts (prevent for confirmation)
ssh-keyscan -t rsa -H bitbucket.org >> ~/.ssh/known_hosts

apt-get -y install git ansible

git clone https://zholka@bitbucket.org/zholka/odroid.git

ansible-playbook  zibis_os_bootstrap_playbook.yml

exit 0
