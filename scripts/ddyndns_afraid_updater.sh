#!/bin/sh
#FreeDNS updater script
#http://freedns.afraid.org/api
#z1b1.uk.to|92.233.108.219|http://freedns.afraid.org/dynamic/update.php?bll2U2FqazB6WlN4UTVCZWlXNnNpUnlqOjE4OTE3ODY2
#zibi.uk.to|92.233.108.219|http://freedns.afraid.org/dynamic/update.php?bll2U2FqazB6WlN4UTVCZWlXNnNpUnlqOjE4OTE3ODY1

UPDATEURL="http://freedns.afraid.org/dynamic/update.php?bll2U2FqazB6WlN4UTVCZWlXNnNpUnlqOjE4OTE3ODY2"
DOMAIN="z1b1.uk.to"

registered=$(nslookup $DOMAIN|tail -n2|grep A|sed s/[^0-9.]//g)
current=$(wget -q -O - http://checkip.dyndns.org|sed s/[^0-9.]//g)

[ "$current" != "$registered" ] && {                           
   wget -q -O /dev/null $UPDATEURL 
   echo "DNS updated on:"; date
}

UPDATEURL="http://freedns.afraid.org/dynamic/update.php?bll2U2FqazB6WlN4UTVCZWlXNnNpUnlqOjE4OTE3ODY1"
DOMAIN="zibi.uk.to"

registered=$(nslookup $DOMAIN|tail -n2|grep A|sed s/[^0-9.]//g)
current=$(wget -q -O - http://checkip.dyndns.org|sed s/[^0-9.]//g)

[ "$current" != "$registered" ] && {                           
   wget -q -O /dev/null $UPDATEURL 
   echo "DNS updated on:"; date
}
